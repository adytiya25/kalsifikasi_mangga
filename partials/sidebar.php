<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="?mnu=home">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <?php
    if ($_SESSION['cstatus'] == "super_admin") { ?>
      <li class="nav-item nav-category">Konfigurasi User</li>
      <li class="nav-item">
        <a class="nav-link" href="?mnu=admin">
          <i class="menu-icon mdi mdi-account-circle-outline"></i>
          <span class="menu-title">Admin</span>
        </a>
      </li>
    <?php } ?>


    <li class="nav-item nav-category">Forms and Datas</li>
    <li class="nav-item">
    <li class="nav-item">
      <a class="nav-link" href="?mnu=dataset">
        <i class="menu-icon mdi mdi-cloud-check"></i>
        <span class="menu-title">Dataset</span>
      </a>
    </li>
    </li>
    <?php ?>
    <li class="nav-item">
      <a class="nav-link" href="?mnu=pengujian">
        <i class="menu-icon mdi mdi-account-box"></i>
        <span class="menu-title">Pengujian</span>
        <i class="menu-arrow"></i>
      </a>
    </li>
    <li class="nav-item nav-category">users</li>
    <li class="nav-item">
      <a class="nav-link" href="?mnu=logout">
        <i class="menu-icon mdi mdi-account-box"></i>
        <span class="menu-title">logout</span>
        <i class="menu-arrow"></i>
      </a>
    </li>
  </ul>
</nav>