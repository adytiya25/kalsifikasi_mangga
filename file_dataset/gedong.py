import numpy as np
import cv2
import os
import MySQLdb
import time
from matplotlib import pyplot as plt
from matplotlib.image import imread
import numpy as np
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt


def setNorm(img, b, c):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = myresize(img, b, c)
    return img


def myresize(img, b, c):
    dim = (b, c)
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return resized


TRAIN_IMG_FOLDER = 'file_dataset/Gedong/latih/'
TEST_IMG_FOLDER = 'file_dataset/Gedong/testing/'

train_set_files = os.listdir(TRAIN_IMG_FOLDER)
test_set_files = os.listdir(TEST_IMG_FOLDER)

width = 128
height = 128

train_id_file = set([f.split('_')[0] for f in train_set_files])
test_id_file = set([f.split('_')[0] for f in train_set_files])
print(train_id_file <= test_id_file)

print('Train Images:')
train_image_names = os.listdir(TRAIN_IMG_FOLDER)
training_tensor = np.ndarray(
    shape=(len(train_image_names), height*width), dtype=np.float64)

for i in range(len(train_image_names)):
    AL = TRAIN_IMG_FOLDER + train_image_names[i]
    img = plt.imread(AL)
    img = setNorm(img, width, height)
    training_tensor[i, :] = np.array(img, dtype='float64').flatten()
    plt.subplot(24, 10, 1+i)
    plt.imshow(img, cmap='gray')
    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
plt.show()

print('Test Images:')
# [i for i in dataset_dir if i not in train_image_names]
test_image_names = os.listdir(TEST_IMG_FOLDER)
testing_tensor = np.ndarray(
    shape=(len(test_image_names), height*width), dtype=np.float64)

print('Train Images:')
train_image_names = os.listdir(TRAIN_IMG_FOLDER)
training_tensor = np.ndarray(
    shape=(len(train_image_names), height*width), dtype=np.float64)

for i in range(len(train_image_names)):
    AL = TRAIN_IMG_FOLDER + train_image_names[i]
    # print(AL)
    img = plt.imread(AL)
    img = setNorm(img, width, height)
    training_tensor[i, :] = np.array(img, dtype='float64').flatten()
    plt.subplot(24, 10, 1+i)
    plt.imshow(img, cmap='gray')
    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
    # print(training_tensor[i,:])
plt.show()

print('Test Images:')
# [i for i in dataset_dir if i not in train_image_names]
test_image_names = os.listdir(TEST_IMG_FOLDER)
testing_tensor = np.ndarray(
    shape=(len(test_image_names), height*width), dtype=np.float64)

for i in range(len(test_image_names)):
    AL = TEST_IMG_FOLDER + test_image_names[i]
    img = imread(AL)
    img = setNorm(img, width, height)
    testing_tensor[i, :] = np.array(img, dtype='float64').flatten()
    plt.subplot(24, 5, 1+i)
    plt.imshow(img, cmap='gray')
    plt.subplots_adjust(right=1.2, top=1.2)
    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
plt.show()

mean_manggo = np.zeros((1, height*width))

for i in training_tensor:
    mean_manggo = np.add(mean_manggo, i)

mean_manggo = np.divide(mean_manggo, float(len(train_image_names))).flatten()

plt.imshow(mean_manggo.reshape(height, width), cmap='gray')
plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                top='off', right='off', left='off', which='both')
plt.show()

normalised_training_tensor = np.ndarray(
    shape=(len(train_image_names), height*width))

for i in range(len(train_image_names)):
    normalised_training_tensor[i] = np.subtract(
        training_tensor[i], mean_manggo)
for i in range(len(train_image_names)):
    img = normalised_training_tensor[i].reshape(height, width)
    plt.subplot(24, 10, 1+i)
    plt.imshow(img, cmap='gray')
    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
plt.show()
cov_matrix = np.cov(normalised_training_tensor)
cov_matrix = np.divide(cov_matrix, 25.0)
print('Covariance Matrix Shape:', cov_matrix.shape)

# eigenvalues and eigenvectors
eigenvalues, eigenvectors, = np.linalg.eig(cov_matrix)
print('eigenvalues.shape: {} eigenvectors.shape: {}'.format(
    eigenvalues.shape, eigenvectors.shape))
eig_pairs = [(eigenvalues[index], eigenvectors[:, index])
             for index in range(len(eigenvalues))]

# Sort the eigen pairs in descending order:
eig_pairs.sort(reverse=True)
eigvalues_sort = [eig_pairs[index][0] for index in range(len(eigenvalues))]
eigvectors_sort = [eig_pairs[index][1] for index in range(len(eigenvalues))]

sorted_ind = sorted(
    range(eigenvalues.shape[0]), key=lambda k: eigenvalues[k], reverse=True)

eigvalues_sort = eigenvalues[sorted_ind]
eigvectors_sort = eigenvectors[sorted_ind]
train_set_files_sort = np.array(train_set_files)[sorted_ind]

var_comp_sum = np.cumsum(eigvalues_sort)/sum(eigvalues_sort)

# Show cumulative proportion of varaince with respect to components
print("Cumulative proportion of variance explained vector: \n%s" % var_comp_sum)

# x-axis for number of principal components kept
num_comp = range(1, len(eigvalues_sort)+1)
plt.title('Cum. Prop. Variance Explain and Components Kept')
plt.xlabel('Principal Components')
plt.ylabel('Cum. Prop. Variance Expalined')

plt.scatter(num_comp, var_comp_sum)
plt.show()

reduced_data = np.array(eigvectors_sort[:25]).transpose()
reduced_data.shape

print(training_tensor.transpose().shape, reduced_data.shape)
proj_data = np.dot(training_tensor.transpose(), reduced_data)
proj_data = proj_data.transpose()
proj_data.shape

for i in range(proj_data.shape[0]):
    img = proj_data[i].reshape(height, width)
    plt.subplot(10, 5, 1+i)
    plt.imshow(img, cmap='gray')
    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
plt.show()

w = np.array([np.dot(proj_data, i) for i in normalised_training_tensor])
print(w.shape)


def recogniser(test_image_names, train_image_names, proj_data, w, t0=2e8, prn=False):

    count = 0
    num_images = 0
    correct_pred = 0

    result = []
    wts = []

    # False match rate (FMR)
    FMR_count = 0

    # False non-match rate (FNMR)
    FNMR_count = 0

    test_image_names2 = sorted(test_image_names)

    for img in test_image_names2:

        unknown_manggo = plt.imread(TEST_IMG_FOLDER+img)
        num_images += 1
        ad = setNorm(unknown_manggo, height, width)
        unknown_manggo_vector = np.array(ad, dtype='float64').flatten()
        normalised_umanggo_vector = np.subtract(
            unknown_manggo_vector, mean_manggo)

        w_unknown = np.dot(proj_data, normalised_umanggo_vector)
        diff = w - w_unknown
        norms = np.linalg.norm(diff, axis=1)
        index = np.argmin(norms)

        wts.append([count, norms[index]])

        if prn:
            print('Input:'+'.'.join(img.split('.')[:2]), end='\t')
        count += 1

        match = img.split('_')[0] == train_image_names[index].split('_')[0]
        if norms[index] < t0:  # It's a manggo
            if match:
                if prn:
                    print('Matched:' + train_image_names[index], end='\t')
                correct_pred += 1
                result.append(1)
            else:
                if prn:
                    print('F/Matched:'+train_image_names[index], end='\t')
                result.append(0)
                FMR_count += 1
        else:
            if match:
                if prn:
                    print('Unknown manggo!'+train_image_names[index], end='\t')
                FNMR_count += 1

            else:
                pass
                correct_pred += 1

        if prn:
            print(norms[index], end=' ')
        if prn:
            print()

    FMR = FMR_count/num_images
    FNMR = FNMR_count/num_images

    print('Correct predictions: {}/{} = {} \t\t'.format(correct_pred,
          num_images, correct_pred/num_images), end=' ')
    print('FMR: {} \t'.format(FMR), end=' ')
    print('FNMR: {} \t'.format(FNMR))

    return wts, result, correct_pred, num_images, FMR, FNMR


wts, result, correct_pred, num_images, FMR, FNMR = recogniser(
    test_image_names, train_image_names, proj_data, w, t0=2e8, prn=True)


def rg(r):
    if r:
        return 'g'
    else:
        return 'r'


cl = [rg(r) for r in result]

x = [x[0] for x in wts]
y = [y[1] for y in wts]
plt.scatter(x, y, color=cl, label='Distance measure (true ang false pred.)')

x2 = [x[0] for x in wts]
y2 = [2.7e7 for y in wts]

plt.plot(x2, y2, label='Empirical error threshold')
plt.legend()
plt.grid()

plt.show()

CPR_list, t0_list, FMR_list, FNMR_list = [], [], [], []
for t0 in np.linspace(start=0, stop=1e8, num=20):
    print('{:e}'.format(t0), end=' ')
    wts, result, correct_pred, num_images, FMR, FNMR = recogniser(
        test_image_names, train_image_names, proj_data, w, t0)

    CPR_list.append(correct_pred/num_images)
    t0_list.append(t0)
    FMR_list.append(FMR)
    FNMR_list.append(FNMR)
x1 = t0_list
y1 = FMR_list

x2 = t0_list
y2 = FNMR_list

x3 = t0_list
y3 = CPR_list

plt.plot(x1, y1, ls='--', color='r', label='FMR',)
plt.plot(x2, y2, ls='-.', color='b', label='FNMR')
plt.plot(x3, y3, color='g', label='Correct prediction using threshold')

plt.grid()
plt.legend()

count = 0
num_images = 0
correct_pred = 0
data_actual = []
data_predictions = []


def Visualization(img, train_image_names, proj_data, w, t0):
    global count, num_images, correct_pred
    unknown_manggo = plt.imread(TEST_IMG_FOLDER+img)
    ad = setNorm(unknown_manggo, height, width)
    num_images += 1
    unknown_manggo_vector = np.array(ad, dtype='float64').flatten()
    normalised_umanggo_vector = np.subtract(unknown_manggo_vector, mean_manggo)

    plt.subplot(240, 2, 1+count)
    plt.imshow(unknown_manggo, cmap='gray')
    plt.title('Input:'+'.'.join(img.split('.')[:2]))
    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
    count += 1

    w_unknown = np.dot(proj_data, normalised_umanggo_vector)
    diff = w - w_unknown
    norms = np.linalg.norm(diff, axis=1)
    index = np.argmin(norms)

    plt.subplot(240, 2, 1+count)
    if norms[index] < t0:  # It's a manggo
        L1 = img.split('_')[0]
        L2 = train_image_names[index].split('_')[0]
        kon1 = 1
        if L1 == 'Mentah':
            kon1 = 0
        data_actual.append(kon1)

        kon2 = 1
        if L2 == 'Mentah':
            kon2 = 0
        data_predictions.append(kon2)

        match = L1 == L2
        # if img.split('.')[0] == train_image_names[index].split('.')[0]:
        if match:
            # plt.title('Matched:'+'.'.join(train_image_names[index].split('.')[:2]), color='g')
            plt.title('Matched:', color='g')
            plt.imshow(imread(TRAIN_IMG_FOLDER +
                       train_image_names[index]), cmap='gray')
            correct_pred += 1
        else:
            # plt.title('Matched:'+'.'.join(train_image_names[index].split('.')[:2]), color='r')
            plt.title('False matched:', color='r')
            plt.imshow(imread(TRAIN_IMG_FOLDER +
                       train_image_names[index]), cmap='gray')

    else:
        # if img.split('.')[0] not in [i.split('.')[0] for i in train_image_names] and img.split('.')[0] != 'apple':

        if img.split('_')[0] not in [i.split('_')[0] for i in train_image_names]:
            plt.title('Unknown manggo', color='g')
            correct_pred += 1
        else:
            plt.title('Unknown manggo', color='r')

    plt.tick_params(labelleft='off', labelbottom='off', bottom='off',
                    top='off', right='off', left='off', which='both')
    plt.subplots_adjust(right=1.2, top=2.5)

    count += 1


fig = plt.figure(figsize=(5, 30))

test_image_names2 = sorted(test_image_names)
for i in range(len(test_image_names2)):
    Visualization(test_image_names2[i],
                  train_image_names, proj_data, w, t0=2.7e1)


plt.show()
print(data_actual)
print(data_predictions)


# menghitung confusion matrix
cm = confusion_matrix(data_actual, data_predictions)

# menampilkan confusion matrix dalam bentuk heatmap
sns.heatmap(cm, annot=True, fmt="d")
plt.ylabel("Target")
plt.xlabel("Prediksi")
plt.show()


