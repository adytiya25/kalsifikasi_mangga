<?php
$pro = "simpan";

$nama_admin = "";
$username = "";
$password = "";
$telepon = "";
$email = "";
$status = "Aktif";
$keterangan = "";
?>

<?php
$sql = "select `id_admin` from `$tbadmin` order by `id_admin` desc";
$jum = getJum($conn, $sql);
$kd = "ADM";
if ($jum > 0) {
	$d = getField($conn, $sql);
	$idmax = $d['id_admin'];
	$urut = substr($idmax, 3, 2) + 1; //01
	if ($urut < 10) {
		$idmax = "$kd" . "0" . $urut;
	} else {
		$idmax = "$kd" . $urut;
	}
} else {
	$idmax = "$kd" . "01";
}
$id_admin = $idmax;
?>

<?php
$lbl = "Tambah Data Admin";
if (isset($_GET["pro"]) && $_GET["pro"] == "ubah") {
	$id_admin = $_GET["kode"];
	$sql = "select * from `$tbadmin` where `id_admin`='$id_admin'";
	$d = getField($conn, $sql);
	$id_admin = $d["id_admin"];
	$id_admin0 = $d["id_admin"];
	$nama_admin = $d["nama_admin"];
	$username = $d["username"];
	$password = $d["password"];
	$telepon = $d["telepon"];
	$email = $d["email"];
	$status = $d["status"];
	$keterangan = $d["keterangan"];
	$lbl = "Ubah Data Admin";
	$pro = "ubah";
}
?>

<h3 style="text-align: center;">
	<?php echo $lbl ?>
</h3>
<form class="forms-sample" action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="nama_admin">Nama Admin</label>
		<input type="text" class="form-control" id="nama_admin" name="nama_admin" value="<?php echo $nama_admin; ?>"
			placeholder="Nama Admin">
	</div>
	<div class="form-group">
		<label for="telepon">Telepon</label>
		<input type="text" class="form-control" id="telepon" name="telepon" value="<?php echo $telepon; ?>"
			placeholder="Telepon">
	</div>
	<div class="form-group">
		<label for="email">Email</label>
		<input class="form-control" name="email" type="email" id="email" value="<?php echo $email; ?>"
			placeholder="Email">
	</div>

	<div class="form-group">
		<label for="username">Username</label>
		<input class="form-control" name="username" id="username" value="<?php echo $username; ?>"
			placeholder="Username">
	</div>

	<div class="form-group">
		<label for="password">password</label>
		<input class="form-control" name="password" type="password" id="password" value="<?php echo $password; ?>"
			placeholder="password">
	</div>

	<div class="form-group">
		<label for="status">Status</label>
		<div>
			<input type="radio" name="status" id="status" checked="checked" value="Aktif" <?php if ($status == "Aktif") {
				echo "checked";
			} ?> />Aktif
			<input type="radio" name="status" id="status" value="Tidak Aktif" <?php if ($status == "Tidak Aktif") {
				echo "checked";
			} ?> />Tidak Aktif
		</div>
	</div>
	<div class="form-group">
		<label for="keterangan">keterangan</label>
		<input class="form-control" name="keterangan" type="text" id="keterangan" value="<?php echo $keterangan; ?>"
			placeholder="keterangan">
	</div>

	<div class="form-group">
		<input name="Simpan" class="btn btn-danger" type="submit" id="Simpan" value="Simpan" />
		<input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
		<input name="id_admin" type="hidden" id="id_admin" value="<?php echo $id_admin; ?>" />
		<input name="id_admin0" type="hidden" id="id_admin0" value="<?php echo $id_admin0; ?>" />
		<a href="?mnu=admin"><input class="btn btn-primary" name="Batal" type="button" id="Batal" value="Batal" /></a>
	</div>

	
</form>


<div class="col-lg-126 grid-margin stretch-card">
	<div class="card">
		<div class="card-body">
			<h4 style="text-align: center;" class="card-title">Data Admin</h4>
			<h4 style="text-align: right;"><button type="button" class="btn btn-info btn-md" title='Print'
					OnClick="PRINT()">
					<i class="ti-printer btn-icon-append"></i>
				</button>
			</h4>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="3%">No</td>
							<th width="7%">ID Admin</td>
							<th width="20%">Nama Admin</td>
							<th width="7%">Telepon</td>
							<th width="15%">Email</td>
							<th width="20%">Keterangan</td>
							<th style="text-align: center;" width="5%">Menu</td>
						</tr>
						<?php
						$sql = "select * from `$tbadmin`  order by `id_admin` desc";
						$jum = getJum($conn, $sql);
						if ($jum > 0) {
							$no = 1;
							$arr = getData($conn, $sql);
							foreach ($arr as $d) {
								$id_admin = $d["id_admin"];
								$nama_admin = ucwords($d["nama_admin"]);
								$username = $d["username"];
								$password = $d["password"];
								$telepon = $d["telepon"];
								$email = $d["email"];
								$status = $d["status"];
								$keterangan = $d["keterangan"];
								?>
							</thead>
							<tbody>
								<?php
								echo "<tr>
				<td>$no</td>
				<td>$id_admin</td>
				<td>$nama_admin</td>
				<td>$telepon</td>
				<td>$email</td>			
				<td>$keterangan</td>
				<td><div align='center'>
				<a href='?mnu=admin&pro=ubah&kode=$id_admin'><button type='button' class='btn btn-primary btn-sm'><i class='mdi mdi-tooltip-edit'></i></button></a>
				<a href='?mnu=admin&pro=hapus&kode=$id_admin'><button type='button' class='btn btn-danger btn-sm'title='hapus' 
				onClick='return confirm(\"Apakah Anda benar-benar akan menghapus \"$nama_admin\" pada data admin ?..\"'><i class='mdi mdi-delete-forever'></i></button></a></div></td>
				</tr>";

								$no++;
							} //for dalam
						} //if
						else {
							echo "<tr><td colspan='6'><blink>Maaf, Data admin belum tersedia...</blink></td></tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php
if (isset($_POST["Simpan"])) {
	$pro = strip_tags($_POST["pro"]);
	$id_admin = strip_tags($_POST["id_admin"]);
	$id_admin0 = strip_tags($_POST["id_admin0"]);
	$username = strip_tags($_POST["username"]);
	$nama_admin = strip_tags($_POST["nama_admin"]);
	$password = strip_tags($_POST["password"]);
	$telepon = strip_tags($_POST["telepon"]);
	$email = strip_tags($_POST["email"]);
	$status = strip_tags($_POST["status"]);
	$keterangan = strip_tags($_POST["keterangan"]);

	if ($pro == "simpan") {
		$sql = " INSERT INTO `$tbadmin` (
					`id_admin` ,
					`nama_admin` ,
					`username` ,
					`password` ,
					`telepon` ,
					`email` ,
					`status` ,
					`keterangan`
					) VALUES (
					'$id_admin', 
					'$nama_admin',
					'$username',
					'$password', 
					'$telepon',
					'$email',
					'$status',
					'$keterangan'
					)";

		$simpan = process($conn, $sql);
		if ($simpan) {
			echo "<script>alert('Data $nama_admin berhasil disimpan !');document.location.href='?mnu=admin';</script>";
		} else {
			echo "<script>alert('Data $nama_admin gagal disimpan...');document.location.href='?mnu=admin';</script>";
		}
	} else {
		$sql = "update `$tbadmin` set 
					`nama_admin`='$nama_admin',
					`username`='$username',
					`password`='$password',
					`telepon`='$telepon' ,
					`email`='$email',
					`status`='$status',
					`keterangan`='$keterangan'
					 where `id_admin`='$id_admin0'";

		$ubah = process($conn, $sql);
		if ($ubah) {
			echo "<script>alert('Data $nama_admin berhasil diubah !');document.location.href='?mnu=admin';</script>";
		} else {
			echo "<script>alert('Data $nama_admin gagal diubah...');document.location.href='?mnu=admin';</script>";
		}
	} //else simpan
}
?>

<?php
if (isset($_GET["pro"]) && $_GET["pro"] == "hapus") {
	$id_admin = $_GET["kode"];
	$sql = "delete from `$tbadmin` where `id_admin`='$id_admin'";
	echo "delete from `$tbadmin` where `id_admin`='$id_admin'";
	$hapus = process($conn, $sql);
	if ($hapus) {
		echo "<script>alert('Data $id_admin berhasil dihapus !');document.location.href='?mnu=admin';</script>";
	} else {
		echo "<script>alert('Data $id_admin gagal dihapus...');document.location.href='?mnu=admin';</script>";
	}
}
?>