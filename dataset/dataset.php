<?php
$pro = "simpan";

$nama_dataset = "";
$katagori = "";
$gambar = "";
$status = "Aktif";
$keterangan = "";
?>

<script type="text/javascript">
	function PRINT(pk) {
		win = window.open('dataset/dataset_print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, status=0');
	}
</script>
<script language="JavaScript">
	function buka(url) {
		window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
	}
</script>

<?php
$lbl = "Tambah Data Dataset";
if (isset($_GET["pro"]) && $_GET["pro"] == "ubah") {
	$id_dataset = $_GET["kode"];
	$sql = "select * from `$tbdataset` where `id_dataset`='$id_dataset'";
	$d = getField($conn, $sql);
	$id_dataset = $d["id_dataset"];
	$id_dataset0 = $d["id_dataset"];
	$nama_dataset = $d["nama_dataset"];
	$katagori = $d["katagori"];
	$gambar = $d["gambar"];
	$status = $d["status"];
	$keterangan = $d["keterangan"];
	$lbl = "Ubah Data Dataset";
	$pro = "ubah";
}
?>

<h3 style="text-align: center;">
	<?php echo $lbl ?>
</h3>
<form action="" method="post" enctype="multipart/form-data">
	<table class="table">
		<tr>
			<th width="70"><label for="nama_dataset">Nama Dataset</label>
			<th width="9">:
			<th colspan="2"><input required name="nama_dataset" class="form-control" type="text" id="nama_dataset"
					value="<?php echo $nama_dataset; ?>" size="25" />
				</b>
		</tr>
		<tr>
			<td height="24"><label for="katagori">Katagori</label>
			<td>:
			<td><input required name="katagori" class="form-control" type="text" id="katagori"
					value="<?php echo $katagori; ?>" size="25" /></td>
		</tr>

		<tr>
			<td height="24"><label for="gambar">Gambar</label>
			<td>:
			<td><input name="gambar" type="file" id="gambar[]" value="<?php echo $gambar; ?>" size="25" multiple></td>
		</tr>
		<tr>
			<td><label for="status">Status</label>
			<td>:
			<td colspan="2">
				<input type="radio" name="status" id="status" checked="checked" value="Aktif" <?php if ($status == "Aktif") {
					echo "checked";
				} ?> />Aktif
				<input type="radio" name="status" id="status" value="Tidak Aktif" <?php if ($status == "Tidak Aktif") {
					echo "checked";
				} ?> />Tidak Aktif
			</td>
		</tr>

		<tr>
			<td height="24"><label for="keterangan">keterangan</label>
			<td>:
			<td><input required name="keterangan" class="form-control" type="text" id="keterangan"
					value="<?php echo $keterangan; ?>" /></td>
		</tr>


		<tr>
			<td>
			<td>
			<td colspan="2">
				<input name="Simpan" class="btn btn-danger" type="submit" id="Simpan" value="Simpan" />
				<input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
				<input name="id_dataset0" type="hidden" id="id_dataset0" value="<?php echo $id_dataset; ?>" />
				<input name="id_dataset" type="hidden" id="id_dataset" value="<?php echo $id_dataset; ?>" />
				<a href="?mnu=dataset"><input class="btn btn-primary" name="Batal" type="button" id="Batal"
						value="Batal" /></a>
			</td>
		</tr>
	</table>
</form>
<div class="col-lg-12 grid-margin stretch-card">
	<div class="card">
		<div class="card-body">
			<h4 style="text-align: right;"><button type="button" class="btn btn-info btn-md" title='Print'
					OnClick="PRINT()">
					<i class="ti-printer btn-icon-append"></i>
				</button>
			</h4>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="3%">No</td>
							<th width="7%">Gambar</td>
							<th width="35%">Detail Dataset</td>
							<th width="5%">Status</td>
							<th style="text-align: center;" width="5%">Menu</td>
						</tr>
						<?php
						$sql = "select * from `$tbdataset` order by `id_dataset` desc";
						$jum = getJum($conn, $sql);
						if ($jum > 0) {
							$no = 1;
							$arr = getData($conn, $sql);
							foreach ($arr as $d) {
								$id_dataset = $d["id_dataset"];
								$nama_dataset = ucwords($d["nama_dataset"]);
								$katagori = $d["katagori"];
								$gambar = $d["gambar"];
								$status = $d["status"];
								$keterangan = $d["keterangan"]; ?>
							</thead>
							<tbody>
								<?php
								echo "<tr>
					<td>$no</td>
					<td><div align='center'>";
								echo "<a href='#' onclick='buka(\"dataset/zoom.php?id=$id_dataset\")'>
									<img src='$YPATH/$gambar' width='40' height='40' /></a></div>";
								echo "</td>
					<td><b>$nama_dataset</b> | Kategori: $katagori<br>Keterangan: $keterangan</td>
					<td>$status</td>
					<td><div align='center'>
					<a href='?mnu=dataset&pro=ubah&kode=$id_dataset'><button type='button' class='btn btn-primary btn-sm'><i class='mdi mdi-tooltip-edit'></i></button></a>
					<a href='?mnu=dataset&pro=hapus&kode=$id_dataset'><button type='button' class='btn btn-danger btn-sm'title='hapus' 
					onClick='return confirm(\"Apakah Anda benar-benar akan menghapus \"$nama_dataset\" pada data dataset ?..\"'><i class='mdi mdi-delete-forever'></i></button></a></div></td>
					</tr>";
								$no++;
							} //for dalam
						} //if
						else {
							echo "<tr><td colspan='6'><blink>Maaf, Data admin belum tersedia...</blink></td></tr>";
						}
						?>
					</tbody>
				</table>
				<?php
				$jmldata = $jum;
				echo "<p align=center>Total data <b>$jmldata</b> item</p>";
				?>
			</div>
		</div>
	</div>
</div>
<?php
if (isset($_POST["Simpan"])) {
	$pro = strip_tags($_POST["pro"]);
	$id_dataset = strip_tags($_POST["id_dataset"]);
	$id_dataset0 = strip_tags($_POST["id_dataset0"]);
	$katagori = strip_tags($_POST["katagori"]);
	$nama_dataset = strip_tags($_POST["nama_dataset"]);
	$status = strip_tags($_POST["status"]);
	$keterangan = strip_tags($_POST["keterangan"]);
	$gambar0 = strip_tags($_POST["gambar0"]);
	if ($_FILES["gambar"] != "") {
		$input_file = $_FILES["gambar"];
		// Menentukan ukuran yang diinginkan (128 pixel)
		$size = 128;
		// Menjalankan file Python untuk memproses resize dan hapus background
		$output = shell_exec("python process_image.py $input_file $size");
		move_uploaded_file($_FILES["gambar[]"]["tmp_name"], "$dataset/" . $_FILES["gambar"]["name"]);
		$gambar = $_FILES["gambar"]["name"];
	} else {
		$gambar = $gambar0;
	}
	if (strlen($gambar) < 1) {
		$gambar = $gambar0;
	}

	if ($pro == "simpan") {
		$sql = " INSERT INTO `$tbdataset` (
					`id_dataset` ,
					`nama_dataset` ,
					`katagori` ,
					`gambar` ,
					`status` ,
					`keterangan`
					) VALUES (
					'$id_dataset', 
					'$nama_dataset',
					'$katagori',
					'$gambar', 
					'$status',
					'$keterangan'
					)";

		$simpan = process($conn, $sql);
		if ($simpan) {
			echo "<script>alert('Data $nama_dataset berhasil disimpan !');document.location.href='?mnu=dataset';</script>";
		} else {
			echo "<script>alert('Data $nama_dataset gagal disimpan...');document.location.href='?mnu=dataset';</script>";
		}
	} else {
		$sql = "UPDATE `$tbdataset` SET 
					`nama_dataset`='$nama_dataset',
					`katagori`='$katagori',
					`gambar`='$gambar',
					`status`='$status',
					`keterangan`='$keterangan'
					 WHERE `id_dataset`='$id_dataset0'";

		$ubah = process($conn, $sql);
		if ($ubah) {
			echo "<script>alert('Data $nama_dataset berhasil diubah !');document.location.href='?mnu=dataset';</script>";
		} else {
			echo "<script>alert('Data $nama_dataset gagal diubah...');document.location.href='?mnu=dataset';</script>";
		}
	} //else simpan
}
?>

<?php
if (isset($_GET["pro"]) && $_GET["pro"] == "hapus") {
	$id_dataset = $_GET["kode"];
	$sql = "delete from `$tbdataset` where `id_dataset`='$id_dataset'";
	$hapus = process($conn, $sql);
	if ($hapus) {
		echo "<script>alert('Data $id_dataset berhasil dihapus !');document.location.href='?mnu=dataset';</script>";
	} else {
		echo "<script>alert('Data $id_dataset gagal dihapus...');document.location.href='?mnu=dataset';</script>";
	}
}
?>