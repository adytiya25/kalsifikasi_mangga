<?php
session_start();
require_once "konmysqli.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>LOGIN</title>
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/typicons/typicons.css">
  <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <h3 style="text-align: center;">login</h3>
                <!-- <img src="images/logo.svg" alt="logo"> -->
              </div>
              <h6 class="fw-light">Sign in to continue.</h6>
              <form  method="post" action="">
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" id="user" name="user" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="pass" name="pass"
                    placeholder="Password">
                </div>
                <div class="mt-3">
                  <button class="btn btn-success" type="submit" name="Login" id="Login">Login</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>
</body>

</html>


<?php
                if (isset($_POST["Login"])) {
                  $usr = $_POST["user"];
                  $pas = $_POST["pass"];

                  $sql1 = "select * from `$tbadmin` where `username`='$usr' and `password`='$pas' and `status`='Aktif'";

                  if (getJum($conn, $sql1) > 0) {
                    $d = getField($conn, $sql1);
                    $kode = $d["id_admin"];
                    $nama = $d["nama_admin"];
                    $email=$d["email"];
                    $status=$d["keterangan"];
                    $_SESSION["cid"] = $kode;
                    $_SESSION["cnama"] = $nama;
                    $_SESSION["cemail"] = $email;
                    $_SESSION["cstatus"] = $status;
                    echo "<script>alert('Otentikasi " . $_SESSION["cstatus"] . " an " . $_SESSION["cnama"] . " (" . $_SESSION["cid"] . ") berhasil Login!');
		document.location.href='index.php?mnu=home';</script>";
                  } else {
                    session_destroy();
                    echo "<script>alert('Otentikasi Login GAGAL !,Silakan cek data Anda kembali...');
			document.location.href='index.php?mnu=login';</script>";
                  }
                }

                ?>