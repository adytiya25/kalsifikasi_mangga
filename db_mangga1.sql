-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jan 2023 pada 13.36
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mangga1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` varchar(15) NOT NULL,
  `nama_admin` varchar(25) NOT NULL,
  `level` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `status` varchar(15) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `nama_admin`, `level`, `email`, `telepon`, `username`, `password`, `status`, `keterangan`) VALUES
('ADM01', 'ady', '', 'adytiya68@gmail.com', '089533985464', 'admin', 'admin', 'Aktif', 'ada1'),
('ADM02', 'saroni', '', 'saroni@gmail.com', '1231231231', 'admin', '1234', 'Aktif', ''),
('ADM03', 'Unde ipsum voluptate', '', 'kyvacyvit@mailinator.com', '90', 'dyvona', 'Pa$$w0rd!', 'Tidak Aktif', 'Enim enim distinctio'),
('ADM04', 'Modi odit ullam quia', '', 'rexyk@mailinator.com', '84', 'donob', 'Pa$$w0rd!', 'Tidak Aktif', 'Aliquip est excepteu'),
('ADM05', 'Quasi et voluptas qu', '', 'libujate@mailinator.com', '73', 'risapohe', 'Pa$$w0rd!', 'Aktif', 'Nostrud assumenda pr');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_dataset`
--

CREATE TABLE `tb_dataset` (
  `id_dataset` int(11) NOT NULL,
  `nama_dataset` varchar(255) NOT NULL,
  `katagori` varchar(255) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `status` varchar(15) NOT NULL,
  `keterangan` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengujian`
--

CREATE TABLE `tb_pengujian` (
  `id_pengujian` varchar(15) NOT NULL,
  `nama_pengujian` varchar(15) NOT NULL,
  `deksripsi` varchar(35) NOT NULL,
  `gambar` varchar(100) NOT NULL DEFAULT 'avatar.jpg',
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `id_admin` varchar(25) NOT NULL,
  `rekapitulasi` varchar(25) NOT NULL,
  `bobot` varchar(25) NOT NULL,
  `hasil` varchar(25) NOT NULL,
  `katagori` varchar(30) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `status` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pengujian`
--

INSERT INTO `tb_pengujian` (`id_pengujian`, `nama_pengujian`, `deksripsi`, `gambar`, `tanggal`, `jam`, `id_admin`, `rekapitulasi`, `bobot`, `hasil`, `katagori`, `keterangan`, `status`) VALUES
('PGJ2301002', 'Perspiciatis te', 'Cumque nulla molesti', 'IMG_20221126_162118.jpg', '2023-01-10', '20:22:34', 'Autem nulla dolores ', 'Tes...Rumus...', '100', 'Et quo itaque quae u', 'Mangga Matang', 'Exercitation consect', 1),
('PGJ2301003', 'Voluptas quasi ', 'Ea doloremque est eu', 'IMG_20221126_162118.jpg', '2023-01-14', '15:35:26', 'ADM01', 'Tes...Rumus...', '100', 'Non id ut blanditiis', 'Mangga Matang', 'Odio dolore sit fuga', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `tb_dataset`
--
ALTER TABLE `tb_dataset`
  ADD PRIMARY KEY (`id_dataset`);

--
-- Indeks untuk tabel `tb_pengujian`
--
ALTER TABLE `tb_pengujian`
  ADD PRIMARY KEY (`id_pengujian`),
  ADD UNIQUE KEY `id_admin` (`id_admin`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_dataset`
--
ALTER TABLE `tb_dataset`
  MODIFY `id_dataset` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
