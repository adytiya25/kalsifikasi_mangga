<?php
$pro = "simpan";

$nama_pengujian = "";
$deksripsi = "";
$gambar = "";
$tanggal = "";
$jam = "";
$id_admin = "";
$rekapitulasi = "";
$bobot = "";
$hasil = "";
$keterangan = "";
?>


<script type="text/javascript">
	function PRINT(pk) {
		win = window.open('pengujian/pengujian_print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, rekapitulasi=0');
	}
</script>
<script language="JavaScript">
	function buka(url) {
		window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
	}
</script>

<?php
$sql = "select `id_pengujian` from `$tbpengujian` order by `id_pengujian` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
	$bl = "0" . $bl;
}

$kd = "PGJ" . $th . $bl; //KEG1610001
if ($jum > 0) {
	$d = mysqli_fetch_array($q);
	$idmax = $d["id_pengujian"];

	$bul = substr($idmax, 5, 2);
	$tah = substr($idmax, 3, 2);
	if ($bul == $bl && $tah == $th) {
		$urut = substr($idmax, 7, 3) + 1;
		if ($urut < 10) {
			$idmax = "$kd" . "00" . $urut;
		} else if ($urut < 100) {
			$idmax = "$kd" . "0" . $urut;
		} else {
			$idmax = "$kd" . $urut;
		}
	} //==
	else {
		$idmax = "$kd" . "001";
	}
} //jum>0
else {
	$idmax = "$kd" . "001";
}
$id_pengujian = $idmax;
?>

<?php
$lbl = "Tambah Data Pengujian";
if (isset($_GET["pro"]) && $_GET["pro"] == "ubah") {
	$id_pengujian = $_GET["kode"];
	$sql = "select * from `$tbpengujian` where `id_pengujian`='$id_pengujian'";
	$d = getField($conn, $sql);
	$id_pengujian = $d["id_pengujian"];
	$id_pengujian0 = $d["id_pengujian"];
	$nama_pengujian = $d["nama_pengujian"];
	$deksripsi = $d["deksripsi"];
	$tanggal = $d["tanggal"];
	$jam = $d["jam"];
	$id_admin = $d["id_admin"];
	$bobot = $d["bobot"];
	$hasil = $d["hasil"];
	$rekapitulasi = $d["rekapitulasi"];
	$keterangan = $d["keterangan"];
	$lbl = "Ubah Data Pengujian";
	$pro = "ubah";
}
?>

<h3 style="text-align: center;">
	<?php echo $lbl ?>
</h3>
<form action="" method="post" enctype="multipart/form-data">
	<table class="table">
		<tr>
			<th width="70"><label for="id_pengujian">ID Pengujian</label>
			<th width="9">:
			<th colspan="2"><b>
					<?php echo $id_pengujian; ?>
				</b>
		</tr>
		<tr>
			<td><label for="nama_pengujian">Nama Pengujian</label>
			<td>:
			<td width="683"><input required name="nama_pengujian" class="form-control" type="text" id="nama_pengujian"
					value="<?php echo $nama_pengujian; ?>" size="25" />
			</td>
		</tr>
		<tr>
			<td height="24"><label for="deksripsi">Deksripsi</label>
			<td>:
			<td><input required name="deksripsi" class="form-control" type="text" id="deksripsi"
					value="<?php echo $deksripsi; ?>" size="25" /></td>
		</tr>
		<tr>
			<td height="24"><label for="gambar">Gambar</label>
			<td>:
			<td><input required name="gambar" type="file" id="gambar" value="<?php echo $gambar; ?>" size="25" /></td>
		</tr>

		<tr>
			<td height="24"><label for="id_admin">Admin</label>
			<td>:
			<td>
				<select class="form-control" name="id_admin">
					<option disabled selected>-- Pilih Admin --</option>
					<?php
					$sql = "select * from `$tbadmin`";
					$arr = getData($conn, $sql);
					foreach ($arr as $d) {
						$id_admin0 = $d["id_admin"];
						$nama_admin = strtoupper($d["nama_admin"]);
						echo "<option value='$id_admin0'";
						if ($id_admin0 == $id_admin) {
							echo "selected";
						}
						echo ">$nama_admin ($id_admin0)</option>";
					}
					?>


				</select>
			</td>
		</tr>

		<tr>
			<td height="24"><label for="rekapitulasi">Rekapitulasi</label>
			<td>:
			<td><input required name="rekapitulasi" class="form-control" type="rekapitulasi" id="rekapitulasi"
					value="<?php echo $rekapitulasi; ?>" size="25" />
			</td>
		</tr>

		<tr>
			<td height="24"><label for="bobot">Bobot</label>
			<td>:
			<td><input required name="bobot" class="form-control" type="bobot" id="bobot" value="<?php echo $bobot; ?>"
					size="25" />
			</td>
		</tr>

		<tr>
			<td height="24"><label for="hasil">Hasil</label>
			<td>:
			<td><input required name="hasil" class="form-control" type="hasil" id="hasil" value="<?php echo $hasil; ?>"
					size="25" />
			</td>
		</tr>

		<tr>
			<td height="24"><label for="keterangan">Keterangan</label>
			<td>:
			<td>
				<textarea name="keterangan" class="form-control" cols="55"
					rows="2"><?php echo $keterangan; ?></textarea>
			</td>
		</tr>

		<tr>
			<td>
			<td>
			<td colspan="2">
				<input name="Simpan" class="btn btn-danger" type="submit" id="Simpan" value="Simpan" />
				<input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
				<input name="id_pengujian" type="hidden" id="id_pengujian" value="<?php echo $id_pengujian; ?>" />
				<input name="id_pengujian0" type="hidden" id="id_pengujian0" value="<?php echo $id_pengujian0; ?>" />
				<a href="?mnu=pengujian"><input class="btn btn-primary" name="Batal" type="button" id="Batal"
						value="Batal" /></a>
			</td>
		</tr>
	</table>
</form>

<!-- Cetak | <img src='ypathicon/print.png' title='PRINT' OnClick="PRINT()">  -->
<div class="col-lg-12 grid-margin stretch-card">
	<div class="card">
		<div class="card-body">
			<h4 style="text-align: center;" class="card-title">Data Pengujian</h4>
			<h4 style="text-align: right;"><button type="button" class="btn btn-info btn-md" title='Print'
					OnClick="PRINT()">
					<i class="ti-printer btn-icon-append"></i>
				</button>
			</h4>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="3%">No</td>
							<th width="7%">Gambar</td>
							<th width="7%">Nama Pengujian</td>
							<th width="7%">Tanggal</td>
							<th width="20%">Rekapitulasi</td>
							<th width="20%">Bobot</td>
							<th width="20%">Hasil</td>
							<th width="20%">kategori</td>
							<th style="text-align: center;" width="5%">Menu</td>
						</tr>
						<?php
						$sql = "select * from `$tbpengujian` order by `id_pengujian` desc";
						$jum = getJum($conn, $sql);
						if ($jum > 0) {
							$no = 1;
							$arr = getData($conn, $sql);
							foreach ($arr as $d) {
								$id_pengujian = $d["id_pengujian"];
								$nama_pengujian = ucwords($d["nama_pengujian"]);
								$deksripsi = $d["deksripsi"];
								$gambar = $d["gambar"];
								$tanggal = WKT($d["tanggal"]);
								$jam = $d["jam"];
								$id_admin = $d["id_admin"];
								$rekapitulasi = $d["rekapitulasi"];
								$bobot = $d["bobot"];
								$hasil = $d["hasil"];
								$keterangan = $d["keterangan"];
								$katagori = $d["katagori"];
								?>
							</thead>
							<tbody>
								<?php
								echo "<tr>
								<td>$no</td>
								<td><div align='center'>";
								echo "<a href='#' onclick='buka(\"pengujian/zoom.php?id=$id_pengujian\")'>
								<img src='$YPATH/$gambar'width='40' height='40'/></a></div>";
								echo "</td>
								<td>$nama_pengujian</td>
								<td>$tanggal $jam</td>
								<td>$rekapitulasi</td>
								<td>$bobot</td>
								<td>$hasil</td>
								<td>$katagori</td>
								<td><div align='center'>
								<a href='?mnu=pengujian&pro=ubah&kode=$id_pengujian'><button type='button' class='btn btn-primary btn-sm'><i class='mdi mdi-tooltip-edit'></i></button></a>
								<a href='?mnu=pengujian&pro=hapus&kode=$id_pengujian'><button type='button' class='btn btn-danger btn-sm'title='hapus' 
								onClick='return confirm(\"Apakah Anda benar-benar akan menghapus \"$nama_pengujian\" pada data pengujian ?..\"'><i class='mdi mdi-delete-forever'></i></button></a></div></td>
								</tr>";
								$no++;
							} //for dalam
						} //if
						else {
							echo "<tr><td colspan='6'><blink>Maaf, Data pengujian belum tersedia...</blink></td></tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>


	<?php
	if (isset($_POST["Simpan"])) {
		$pro = strip_tags($_POST["pro"]);
		$id_pengujian = strip_tags($_POST["id_pengujian"]);
		$id_pengujian0 = strip_tags($_POST["id_pengujian0"]);
		$deksripsi = strip_tags($_POST["deksripsi"]);
		$gambar0 = strip_tags($_POST["gambar0"]);
		if ($_FILES["gambar"] != "") {
			move_uploaded_file($_FILES["gambar"]["tmp_name"], "$YPATH/" . $_FILES["gambar"]["name"]);
			$gambar = $_FILES["gambar"]["name"];
		} else {
			$gambar = $gambar0;
		}
		if (strlen($gambar) < 1) {
			$gambar = $gambar0;
		}
		$nama_pengujian = strip_tags($_POST["nama_pengujian"]);
		$tanggal = date("Y-m-d");
		$jam = date("H:i:s");
		$id_admin = strip_tags($_POST["id_admin"]);
		$rekapitulasi = strip_tags($_POST["rekapitulasi"]);
		$bobot = strip_tags($_POST["bobot"]);
		$hasil = strip_tags($_POST["hasil"]);
		$keterangan = strip_tags($_POST["keterangan"]);

		if ($pro == "simpan") {
			$sql = " INSERT INTO `$tbpengujian` (
					`id_pengujian` ,
					`nama_pengujian` ,
					`deksripsi` ,
					`gambar` ,
					`tanggal` ,
					`jam` ,
					`id_admin` ,
					`rekapitulasi` ,`bobot` ,`hasil` ,
					`keterangan`
					) VALUES (
					'$id_pengujian', 
					'$nama_pengujian',
					'$deksripsi',
					'$gambar',
					'$tanggal', 
					'$jam',
					'$id_admin',
					'$rekapitulasi','$bobot','$hasil',
					'$keterangan'
					)";

			$simpan = process($conn, $sql);
			if ($simpan) {
				echo "<script>alert('Data $nama_pengujian berhasil disimpan !');document.location.href='?mnu=pengujian';</script>";
			} else {
				echo "<script>alert('Data $nama_pengujian gagal disimpan...');document.location.href='?mnu=pengujian';</script>";
			}
		} else {
			$sql = "update `$tbpengujian` set 
					`nama_pengujian`='$nama_pengujian',
					`deksripsi`='$deksripsi',
					`gambar`='$gambar',
					`tanggal`='$tanggal',
					`jam`='$jam' ,
					`id_admin`='$id_admin',
					`rekapitulasi`='$rekapitulasi',
					`bobot`='$bobot',
					`hasil`='$hasil',
					`keterangan`='$keterangan'
					 where `id_pengujian`='$id_pengujian0'";

			$ubah = process($conn, $sql);
			if ($ubah) {
				echo "<script>alert('Data $nama_pengujian berhasil diubah !');document.location.href='?mnu=pengujian';</script>";
			} else {
				echo "<script>alert('Data $nama_pengujian gagal diubah...');document.location.href='?mnu=pengujian';</script>";
			}
		} //else simpan
	}
	?>

	<?php
	if (isset($_GET["pro"]) && $_GET["pro"] == "hapus") {
		$id_pengujian = $_GET["kode"];
		$sql = "delete from `$tbpengujian` where `id_pengujian`='$id_pengujian'";
		$hapus = process($conn, $sql);
		if ($hapus) {
			echo "<script>alert('Data $id_pengujian berhasil dihapus !');document.location.href='?mnu=pengujian';</script>";
		} else {
			echo "<script>alert('Data $id_pengujian gagal dihapus...');document.location.href='?mnu=pengujian';</script>";
		}
	}
	?>