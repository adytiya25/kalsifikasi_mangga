<style type="text/css">
	body {
		width: 100%;
	}
</style>

<body OnLoad="window.print()" OnFocus="window.close()">
	<?php
	include "../konmysqli.php";
	echo "<link href='../ypathcss/$css' rel='stylesheet' type='text/css' />";
	$YPATH = "../ypathfile/";
	$pk = "";
	$field = "id_pengujian";
	$TB = $tbpengujian;
	$item = "Pengujian";



	$sql = "select * from `$TB` order by `$field` asc";
	if (isset($_GET["pk"])) {
		$pk = $_GET["pk"];
		$sql = "select * from `$TB` where `$field`='$pk' order by `$field` asc";
	}

	echo "<h3><center>Laporan Data $item $pk</h3>";
	?>


	<table width="98%" border="0">
		<tr>
			<th width="3%">No</td>
			<th width="10%">ID Pengujian</td>
			<th width="20%">Tanggal</td>
			<th width="20%">Admin</td>
			<th width="20%">Rekapitulasi</td>
			<th width="7%">Gambar</td>
			<th width="20%">Bobot</td>
			<th width="20%">Hasil</td>
		</tr>
		<?php
		$jum = getJum($conn, $sql);
		$no = 0;
		if ($jum > 0) {
			$arr = getData($conn, $sql);
			foreach ($arr as $d) {
				$no++;
				$id_pengujian = $d["id_pengujian"];
				$nama_pengujian = ucwords($d["nama_pengujian"]);
				$deksripsi = $d["deksripsi"];
				$tanggal = $d["tanggal"];
				$jam = $d["jam"];
				$id_admin = $d["id_admin"];
				$rekapitulasi = $d["rekapitulasi"];
				$gambar = $d["gambar"];
				$bobot = $d["bobot"];
				$hasil = $d["hasil"];
				$keterangan = $d["keterangan"];

				$color = "#dddddd";
				if ($no % 2 == 0) {
					$color = "#eeeeee";
				}
				echo "<tr bgcolor='$color'>
<td>$no</td>
<td>$id_pengujian</td>
<td>$nama_pengujian</td>
<td>$tanggal $jam</td>
<td>$rekapitulasi</td>
<td><div align='center'>";
				echo "<a href='#' onclick='buka(\"pengujian/zoom.php?id=$id_pengujian\")'>
<img src='../ypathfile/$gambar' width='40' height='40' /></a></div>";
				echo "</td>			
<td>$bobot</td>
<td>$hasil</td>
				</tr>";
			}
		} //if
		else {
			echo "<tr><td colspan='7'><blink>Maaf, Data $item belum tersedia...</blink></td></tr>";
		}

		echo "</table>";
		?>